package com.covisint.demo.processor;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.covisint.demo.bean.RawMessage;

/**
 * Unmarshal xml to java object(RawMessage)
 */
public class MappingProcessor {

	public List<RawMessage> convertToRawMessage(List<String> listStrData) throws Exception {
		List<RawMessage> listRawMessage = new ArrayList<RawMessage>();

		for (String strXML : listStrData) {
			// Mapping to RawData.
			JAXBContext jaxbContext = JAXBContext.newInstance(RawMessage.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			RawMessage rawMessage = (RawMessage) jaxbUnmarshaller.unmarshal(new StringReader(strXML));

			listRawMessage.add(rawMessage);
		}

		return listRawMessage;
	}

}
