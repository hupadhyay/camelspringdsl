package com.covisint.demo.processor;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import com.covisint.demo.bean.DataHubMessage;
import com.covisint.demo.utils.TransformUtils;

import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

/**
 * Convert DatahubMessage to JSON representative and sent to kafka topic
 */
public class PostProcessor {
	private ObjectMapper objectMapper = null;
	
	public PostProcessor() {
		objectMapper  = new ObjectMapper();
	}
	
	/**
	 * Convert datahub message to json format and send it to kafka TEST topic.
	 * @param listDataHubMessage
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public void sendToKafka(List<DataHubMessage> listDataHubMessage) throws JsonGenerationException, JsonMappingException, IOException{
		ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
		
		Properties properties = new Properties();
		properties.put("metadata.broker.list", TransformUtils.getProperty("dest_server"));
		properties.put("serializer.class", TransformUtils.getProperty("serializer_class"));
		ProducerConfig producerConfig = new ProducerConfig(properties);
		kafka.javaapi.producer.Producer<String, String> producer = new kafka.javaapi.producer.Producer<String, String>(
				producerConfig);
		
		for(DataHubMessage dataHubMessage : listDataHubMessage){
			String dataHubMsgJson = objectWriter.writeValueAsString(dataHubMessage);
			System.out.println(dataHubMsgJson);
			KeyedMessage<String, String> message = new KeyedMessage<String, String>(TransformUtils.getProperty("dest_topic"), dataHubMsgJson);
			producer.send(message);
		}	
		
		producer.close();
	}

}
