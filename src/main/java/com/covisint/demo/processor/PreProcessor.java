package com.covisint.demo.processor;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.apache.log4j.Logger;

import com.covisint.demo.utils.TransformUtils;

/**
 * Decode the encoded value of device id.
 */
public class PreProcessor{
	
	static Logger log = Logger.getLogger(PreProcessor.class.getName());
	
	/**
	 * Read each record of file and and decode deviceid if it is encoded.
	 * @param strData
	 * @return
	 * @throws Exception
	 */
	public List<String> readData(String strData) throws Exception {
		
		String []strLineArr = strData.split("\n");
		String []strToken = null;
		boolean isFirstLine = true;
		
		List<String> listRawData = new ArrayList<String>();
		log.info("Reading of data from raw file.");
		
		for(String strLine : strLineArr){
			
			// Skip First line as it is header of data.
			if(isFirstLine){
				isFirstLine = false;
				continue;
			}
			
			// Take deviceId from record
			strToken = strLine.split(",");
			String strDevice = strToken[1];
			
			// Check whether deviceId is encoded or not. If it is decode to normal string.
			if(TransformUtils.isBase64(strDevice.trim())){
				byte[] bytes = Base64.getDecoder().decode(strDevice.getBytes());
				strToken[1] = new String(bytes);
				
				log.info("Decoding of device id: "+ strDevice);
				
				strLine = TransformUtils.convertIntoCsv(strToken);
			}
			System.out.println(strLine);
				
			listRawData.add(strLine);
		}
		
		return listRawData;
	}

}
