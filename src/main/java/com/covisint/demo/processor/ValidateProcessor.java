package com.covisint.demo.processor;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import com.covisint.demo.utils.TransformUtils;

/**
 * Validating XML against XSD file.
 */
public class ValidateProcessor {

	/**
	 * 
	 * @param listXmlData
	 * @return
	 * @throws Exception
	 */
	public List<String> validate(List<String> listXmlData) throws Exception {

		Iterator<String> iterator = listXmlData.iterator();

		while (iterator.hasNext()) {
			String strXml = iterator.next();

			if (!validateXMLSchema(strXml)) {
				iterator.remove();
			} else {
				System.out.println("Validation is successfull!");
			}
		}

		return listXmlData;

	}

	/**
	 * 
	 * @param xmlDatahub
	 * @return
	 */
	public boolean validateXMLSchema(String xmlDatahub) {
		StringReader stringReader = new StringReader(xmlDatahub);
		try {
			SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = factory.newSchema(new File(TransformUtils.getProperty("validate_xsd")));
			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(stringReader));
		} catch (IOException e) {
			System.out.println("Exception: " + e.getMessage());
			return false;
		} catch (SAXException e1) {
			System.out.println("SAX Exception: " + e1.getMessage());
			return false;
		}
		return true;
	}
}
