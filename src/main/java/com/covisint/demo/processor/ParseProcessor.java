package com.covisint.demo.processor;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 * Convert CSV record to its XML equivalent.
 */
public class ParseProcessor{
	final private String PARENT_NODE = "deviceMsg";
	final private String RELAM = "realm";
	final private String DEICE_ID = "deviceId";
	final private String CMD_ID = "cmdId";
	final private String MSG_ID = "msgId";
	final private String MSG = "msg";
	
	/**
	 * Parse each record of file and create XML documents.
	 * 
	 * @param exchange
	 * @throws Exception
	 */
	public List<String> parseToXML(List<String> listStrData) throws Exception{
		List<String> listDataXml = new ArrayList<String>();
		
		String [] strToken = null;
		for(String strLine : listStrData){
			
			strToken = strLine.split(",");
			String strXml = makeXml(strToken);
			System.out.println(strXml);
			listDataXml.add(strXml);
		}
		
		return listDataXml; 
	}

	/**
	 * Make XML document.
	 * 
	 * @param strToken
	 * @throws XMLStreamException 
	 * @throws IOException 
	 */
	private String makeXml(String[] strToken) throws XMLStreamException, IOException {
		StringWriter stringWriter = new StringWriter();

        XMLOutputFactory xMLOutputFactory = XMLOutputFactory.newInstance();	
        XMLStreamWriter xMLStreamWriter = xMLOutputFactory.createXMLStreamWriter(stringWriter);
  
        xMLStreamWriter.writeStartDocument();
        xMLStreamWriter.writeStartElement(PARENT_NODE);
  
        xMLStreamWriter.writeStartElement(RELAM);			
        xMLStreamWriter.writeCharacters(strToken[0]);
        xMLStreamWriter.writeEndElement();

        xMLStreamWriter.writeStartElement(DEICE_ID);			
        xMLStreamWriter.writeCharacters(strToken[1]);
        xMLStreamWriter.writeEndElement();
        
        xMLStreamWriter.writeStartElement(CMD_ID);			
        xMLStreamWriter.writeCharacters(strToken[2]);
        xMLStreamWriter.writeEndElement();
        
        xMLStreamWriter.writeStartElement(MSG_ID);			
        xMLStreamWriter.writeCharacters(strToken[3]);
        xMLStreamWriter.writeEndElement();
        
        xMLStreamWriter.writeStartElement(MSG);			
        xMLStreamWriter.writeCharacters(strToken[4]);
        xMLStreamWriter.writeEndElement();

        xMLStreamWriter.writeEndElement();
        xMLStreamWriter.writeEndDocument();

        xMLStreamWriter.flush();
        xMLStreamWriter.close();

        String xmlString = stringWriter.getBuffer().toString();

        stringWriter.close();

        return xmlString;
	}
}
