package com.covisint.demo.processor;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.covisint.demo.bean.DataHubMessage;
import com.covisint.demo.bean.RawMessage;
import com.covisint.demo.utils.TransformUtils;

/**
 * Mapping of RawMessage to DatahubMessage also make http call to device service to get eventtemplateid and supportedCommands. 
 */
public class MappingDatahubProcess {

	private List<DataHubMessage> listDataHubMessage = new ArrayList<DataHubMessage>();

	public List<DataHubMessage> mapToDatahub(List<RawMessage> listRawMsg){
		
		for (RawMessage rawMessage : listRawMsg) {
			DataHubMessage dataHubMsg = new DataHubMessage();
			if (fetchDetails(dataHubMsg, rawMessage)) {
				dataHubMsg.setRealm(rawMessage.realm);
				dataHubMsg.setDeviceId(rawMessage.deviceId);
				dataHubMsg.getSenderWirePayload().setCreation(System.currentTimeMillis());
				dataHubMsg.getSenderWirePayload().setCreator("HKU");
				dataHubMsg.getSenderWirePayload().setEncodingType("UTF-8");
				dataHubMsg.getSenderWirePayload().setDeviceId(rawMessage.deviceId);
				dataHubMsg.getSenderWirePayload().setMessage(rawMessage.message);
				dataHubMsg.getSenderWirePayload().setMessageId(rawMessage.customerMessageID);
				dataHubMsg.setCustomerMessageID(rawMessage.customerMessageID);
				
				listDataHubMessage.add(dataHubMsg);
			} else {
				boolean boolContinueFailure = Boolean.parseBoolean(TransformUtils.getProperty("continue_failure"));

				if (boolContinueFailure) {
					continue;
				} else {
					break;
				}
			}
		}
		return listDataHubMessage;
	}

	/**
	 * Query Device service to get device information in json format using Http request.
	 * @param dataHubMsg
	 * @param rawMessage
	 * @return
	 */
	private boolean fetchDetails(DataHubMessage dataHubMsg, RawMessage rawMessage) {

		Client client = ClientBuilder.newClient(new ClientConfig().register(String.class));
		WebTarget webTarget = client.target(TransformUtils.getProperty("driver_url")).path(rawMessage.deviceId);

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON)
				.accept("application/vnd.com.covisint.platform.device.v2+json")
				.header("Content-Type", "application/vnd.com.covisint.platform.device.v2+json")
				.header("x-realm", "IOT_SOL_QA").header("x-requestor", "hku").header("x-requestor-app", "Postman");
		Response response = invocationBuilder.get();

		String strResponse = response.readEntity(String.class);

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject;
		try {
			jsonObject = (JSONObject) jsonParser.parse(strResponse);
			System.out.println(jsonObject.toJSONString());

			if (jsonObject.get("status") != null) {
				return false;
			} else {
				String eventTemplate = (String) ((JSONArray) jsonObject.get("observableEvents")).get(0);
				String cmdTemplate = (String) ((JSONArray) jsonObject.get("supportedCommands")).get(0);
				dataHubMsg.setCmdTemplateId(cmdTemplate);
				dataHubMsg.setEventTemplateId(eventTemplate);
				dataHubMsg.getSenderWirePayload().setEventTemplateId(eventTemplate);
				return true;
			}
		} catch (ParseException e) {
			return false;
		}

	}

}
