package com.covisint.demo.utils;

import java.io.IOException;
import java.util.Properties;

/**
 * Utility class for reading property file and some other utility methods.
 * 
 */
public class TransformUtils {

	private static Properties prop = new Properties();
	
	static{
		try {
			prop.load(ClassLoader.getSystemResourceAsStream("transform.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	public static String getProperty(String key){
		if(key != null && key.length() != 0){
			return prop.getProperty(key);
		}
		return null;
	}
	
	/**
	 * Check for encoding
	 * @param stringBase64
	 * @return
	 */
	public static boolean isBase64(String stringBase){
//        String regex =
//               "([A-Za-z0-9+/]{4})*"+
//               "([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)";
// 
//        Pattern patron = Pattern.compile(regex);
// 
//        if (!patron.matcher(stringBase).matches()) {
//            return false;
//        } else {
//            return true;
//        }
		
		if(stringBase.indexOf("-") != -1){
			return false;
		} else {
			return true;
		}
	}

	/**
	 * convert array to CSV file.
	 * @param strToken
	 */
	public static String convertIntoCsv(String[] strToken) {
		StringBuilder sb = new StringBuilder();
		int count  = 0;
		for(String str : strToken){
			sb.append(str);
			if(count != (strToken.length - 1)){
				sb.append(",");
			}
			count ++;
		}
		return sb.toString();
	}
	
	
}
