package com.covisint.demo.bean;

/**
 * 
 * 
 */
public class SenderWirePayload {
	private String creator;
	private long creation;
	private String messageId;
	private String eventTemplateId;
	private String deviceId;
	private String message;
	private String encodingType;

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public long getCreation() {
		return creation;
	}

	public void setCreation(long creation) {
		this.creation = creation;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getEventTemplateId() {
		return eventTemplateId;
	}

	public void setEventTemplateId(String eventTemplateId) {
		this.eventTemplateId = eventTemplateId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getEncodingType() {
		return encodingType;
	}

	public void setEncodingType(String encodingType) {
		this.encodingType = encodingType;
	}
}
