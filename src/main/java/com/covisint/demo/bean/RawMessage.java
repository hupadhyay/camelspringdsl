package com.covisint.demo.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * 
 */
@XmlRootElement(name="deviceMsg")
public class RawMessage {
	
	/** The realm of the dataHubMessage resource. */
	@XmlElement(name="realm")
    public String realm;
    
	/** Device id of the device. */
	@XmlElement(name="deviceId")
    public String deviceId;
    
    /** Id of a command/event to send to device. */
	@XmlElement(name="cmdId")
    public String commandEventId;
    
    /** Id of a command to send to device. */
	@XmlElement(name="msgId")
    public String customerMessageID;
    
    /** The actual event message payload sent from a device to application. */
    @XmlElement(name="msg")
    public String message;

	String getRealm() {
		return realm;
	}

	void setRealm(String realm) {
		this.realm = realm;
	}

	String getDeviceId() {
		return deviceId;
	}

	void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	String getCommandEventId() {
		return commandEventId;
	}

	void setCommandEventId(String commandEventId) {
		this.commandEventId = commandEventId;
	}

	String getCustomerMessageID() {
		return customerMessageID;
	}

	void setCustomerMessageID(String customerMessageID) {
		this.customerMessageID = customerMessageID;
	}

	String getMessage() {
		return message;
	}

	void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "RawMessage [realm=" + realm + ", deviceId=" + deviceId + ", commandEventId=" + commandEventId
				+ ", customerMessageID=" + customerMessageID + ", message=" + message + "]";
	}
	
}
