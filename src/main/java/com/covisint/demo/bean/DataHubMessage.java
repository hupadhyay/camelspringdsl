package com.covisint.demo.bean;

/**
 * 
 * 
 */
public class DataHubMessage {
	
	/** The realm of the dataHubMessage resource. */
    private String realm;
    
	/** Device id of the device. */
    private String deviceId;
    
    /** Unique id of the event template. */
    private String eventTemplateId;
    
    /** Unique id of command template. */
    private String cmdTemplateId;
    
    /** Id of a command to send to device. */
    private String customerMessageID;
    
    /** The payload of the dataHubMessage resource. */
    private SenderWirePayload senderWirePayload;
    
    /**
     * 
     */
    public DataHubMessage(){
    	senderWirePayload = new SenderWirePayload();
    }
    
	public String getRealm() {
		return realm;
	}

	public void setRealm(String realm) {
		this.realm = realm;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getEventTemplateId() {
		return eventTemplateId;
	}

	public void setEventTemplateId(String eventTemplateId) {
		this.eventTemplateId = eventTemplateId;
	}

	public String getCustomerMessageID() {
		return customerMessageID;
	}

	public void setCustomerMessageID(String customerMessageID) {
		this.customerMessageID = customerMessageID;
	}

	public String getCmdTemplateId() {
		return cmdTemplateId;
	}

	public void setCmdTemplateId(String cmdTemplateId) {
		this.cmdTemplateId = cmdTemplateId;
	}

	public SenderWirePayload getSenderWirePayload() {
		return senderWirePayload;
	}

	public void setSenderWirePayload(SenderWirePayload senderWirePayload) {
		this.senderWirePayload = senderWirePayload;
	}

	@Override
	public String toString() {
		return "DataHubMessage [realm=" + realm + ", deviceId=" + deviceId + ", eventTemplateId=" + eventTemplateId
				+ ", cmdTemplateId=" + cmdTemplateId + ", customerMessageID=" + customerMessageID
				+ ", senderWirePayload=" + senderWirePayload + "]";
	}
	
}
